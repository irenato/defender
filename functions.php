<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.07.17
 * Time: 0:15
 */

include ('includes/includes.php');
include ('includes/custom_posts.php');
include ('includes/hidden.php');
include ('lib/sms.php');
include ('includes/ajx.php');

add_theme_support('post-thumbnails');

function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

/**
 * @param $mimes
 * @return mixed
 * for svg
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

