<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.07.17
 * Time: 0:09
 */

if (is_front_page()) {
    get_template_part('home');
} else if (is_single()) {
    get_template_part('single');
}

