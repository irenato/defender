<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 30.07.17
 * Time: 2:16
 */

add_action('init', 'sections_register');
function sections_register()
{
    register_post_type('reviews',
        array(
            'label' => __('Отзывы'),
            'singular_label' => 'review',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-id-alt',
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
            ),
        )
    );
}
