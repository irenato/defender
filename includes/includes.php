<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.07.17
 * Time: 0:23
 */

function defender_styles()
{
    wp_enqueue_style('defender_fonts', get_template_directory_uri() . '/css/fonts.min.css', '', '', 'all');
    wp_enqueue_style('defender-carousel', get_template_directory_uri() . '/libs/owl.carousel/dist/assets/owl.carousel.min.css', '', '', 'all');
    wp_enqueue_style('defender-carousel_carousel', get_template_directory_uri() . '/libs/owl.carousel/dist/assets/owl.theme.default.min.css', '', '', 'all');
    wp_enqueue_style('defender-animate', get_template_directory_uri() . '/libs/owl.carousel/docs/assets/css/animate.css', '', '', 'all');
    wp_enqueue_style('defender-header', get_template_directory_uri() . '/css/header.min.css', '', '', 'all');
    wp_enqueue_style('defender-main', get_template_directory_uri() . '/css/main.min.css', '', '', 'all');
    wp_enqueue_style('defender-footer', get_template_directory_uri() . '/css/footer.min.css', '', '', 'all');

}

//add_action('wp_enqueue_scripts', 'defender_styles');
add_action('get_footer', 'defender_styles');

function defender_scripts()
{
    wp_enqueue_script('defender-google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBCJX6N-PGyDT2drFx6dbvMhzLljyp4dn4&callback=initMap', false, '', true);
    wp_enqueue_script('defender-jquery', 'https://code.jquery.com/jquery-3.2.1.min.js', false, '', true);
    wp_enqueue_script('defender-carousel-script', get_template_directory_uri() . '/libs/owl.carousel/dist/owl.carousel.min.js', false, '', true);
    wp_enqueue_script('defender-maskedinput-script', get_template_directory_uri() . '/js/maskedinput.min.js', false, '789', true);
    wp_enqueue_script('defender-common', get_template_directory_uri() . '/js/common.js', false, '', true);
    wp_localize_script('defender-common', 'defender_ajax', array(
        'ajax_url' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_enqueue_scripts', 'defender_scripts');