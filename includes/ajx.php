<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 15.08.17
 * Time: 22:04
 */

/*
 * sms
 */
function sms_admin()
{
    $smsru = new SMSRU(get_field('api_key_sms', 9)); // Ваш уникальный программный ключ, который можно получить на главной странице

    $data = new stdClass();
    $data->to = get_field('api_sms_phone', 9);
    $data->text = 'defender. Пользователь ' . $_POST['phone'] . ' запросил обратный звонок'; // Текст сообщения
// $data->from = ''; // Если у вас уже одобрен буквенный отправитель, его можно указать здесь, в противном случае будет использоваться ваш отправитель по умолчанию
// $data->time = time() + 7*60*60; // Отложить отправку на 7 часов
// $data->translit = 1; // Перевести все русские символы в латиницу (позволяет сэкономить на длине СМС)
// $data->test = 1; // Позволяет выполнить запрос в тестовом режиме без реальной отправки сообщения
// $data->partner_id = '1'; // Можно указать ваш ID партнера, если вы интегрируете код в чужую систему
    $sms = $smsru->send_one($data); // Отправка сообщения и возврат данных в переменную
}

add_action('wp_ajax_nopriv_sms_admin', 'sms_admin');
add_action('wp_ajax_sms_admin', 'sms_admin');