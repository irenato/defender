<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 30.07.17
 * Time: 0:35
 */


get_header();

    ?>


    <section class="top-block"
             style="background-image: url('<?= get_template_directory_uri() ?>/images/topBlock2.png')">
        <div class="wrapper-centered">
            <h1>404</h1>
            <p>Страница не найдена</p>
            <a href="<?= get_home_url() ?>" class="btn yellow-main">на главную</a>
        </div>
    </section>

<?php
get_footer();

