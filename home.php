<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 30.07.17
 * Time: 0:35
 */
get_header();
if (have_posts()) : while (have_posts()) :
    the_post();
    $page_id = $post->ID;
    ?>
    <section class="top-block"
             style="background-image: url('<?= get_template_directory_uri() ?>/images/topBlock2.png')">
        <div class="wrapper-centered">
            <?php the_content(); ?>
            <a href="#formConsultation" class="btn yellow-main ancLinks">получить консультацию</a>
        </div>
    </section>
    <section class="how-it-work" id="aboutUs">
        <div class="wrapper">
            <h2><?= get_field('how_it_works_title') ?></h2>
            <?php $items = get_field('how_it_works');
            $i = 0;
            if ($items): ?>
                <ul class="hiw-list">
                    <?php foreach ($items as $item): ?>
                        <li>
                            <span><?= ++$i; ?></span>
                            <p><?= $item['title'] ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>
    <section class="how-it-work-shem mobile-hidden" id="services">
        <div class="wrapper">
            <ul class="shem">
                <li class="shem-item">
                    <ul class="shem-title">
                        <li class="shem-title-item">
                            <img src="<?= get_template_directory_uri() ?>/images/file.svg" alt="">
                            <p>Трудовой договор</p>
                        </li>
                    </ul>
                    <ul class="shem-description">
                        <li class="shem-description-item">
                            <img src="<?= get_template_directory_uri() ?>/images/workers-team.svg" alt="">
                            <p>Работодатель</p>
                            <ul>
                                <li>кадровый учет</li>
                                <li>начисление налогов</li>
                                <li>общениe с госорганами</li>
                                <li>риски, ответственность</li>
                            </ul>
                        </li>
                        <li class="shem-description-item">
                            <img src="<?= get_template_directory_uri() ?>/images/carpenter.svg" alt="">
                            <p>Сотрудник</p>
                            <ul>
                                <li>выполнение своих обычных функций</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="shem-item">
                    <ul class="shem-title">
                        <li class="shem-title-item">
                            <img src="<?= get_template_directory_uri() ?>/images/list.svg" alt="">
                            <p>Аутстаффинг</p>
                        </li>
                        <li class="shem-title-item">
                            <img src="<?= get_template_directory_uri() ?>/images/file.svg" alt="">
                            <p>Трудовой договор</p>
                        </li>
                    </ul>
                    <ul class="shem-description">
                        <li class="shem-description-item">
                            <img src="<?= get_template_directory_uri() ?>/images/workers-team.svg" alt="">
                            <p>Работодатель</p>
                        </li>
                        <li class="shem-description-item">
                            <img src="<?= get_template_directory_uri() ?>/images/workplace.svg" alt="">
                            <p>Аутстаффер</p>
                            <ul>
                                <li>кадровый учет</li>
                                <li>начисление налогов</li>
                                <li>общениe с госорганами</li>
                                <li>риски, ответственность</li>
                            </ul>
                        </li>
                        <li class="shem-description-item">
                            <img src="<?= get_template_directory_uri() ?>/images/carpenter.svg" alt="">
                            <p>Сотрудник</p>
                            <ul>
                                <li>выполнение своих обычных функций</li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </section>
    <section class="form-consultation" id="formConsultation">
        <div class="wrapper">
            <img src="<?= get_template_directory_uri() ?>/images/icon.svg" alt="Defender">
            <h2>ЕСТЬ ВОПРОСЫ?</h2>
            <p>ПОЛУЧИТЕ БЕСПЛАТНУЮ КОНСУЛЬТАЦИЮ</p>
            <?= do_shortcode('[contact-form-7 id="76"]') ?>
        </div>
    </section>
    <section class="whom" style="background-image: url('<?= get_field('auts_image') ?>')">
        <div class="wrapper">
            <h2><?= get_field('auts_title') ?></h2>
            <?php $items = get_field('auts');
            if ($items): ?>
                <ul class="mobile-carusel owl-carousel owl-theme">
                    <?php foreach ($items as $item): ?>
                        <li>
                            <img src="<?= $item['image'] ?>" alt="<?= $item['text'] ?>">
                            <p><?= $item['text'] ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>
    <section class="drop">
        <div class="wrapper">
            <h2><?= get_field('gains_title') ?></h2>
            <?php $items = get_field('gains');
            if ($items): ?>
                <ul>
                    <?php foreach ($items as $item): ?>
                        <li>
                            <img src="<?= $item['image'] ?>" alt="<?= $item['text'] ?>">
                            <p><?= $item['text'] ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>
    <section class="advantages" id="whyWe">
        <div class="left">
            <h2><?= get_field('minus_title') ?></h2>
            <?php $items = get_field('minus');
            if ($items): ?>
                <ul>
                    <?php foreach ($items as $item): ?>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/dislike.svg"
                                 alt="<?= $item['text'] ?>">
                            <p><?= $item['text'] ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="right">
            <h2><?= get_field('plus_title') ?></h2>
            <?php $items = get_field('pluses');
            if ($items): ?>
                <ul>
                    <?php foreach ($items as $item): ?>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/like.svg"
                                 alt="<?= $item['text'] ?>">
                            <p><?= $item['text'] ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>
<?php endwhile;
endif;
wp_reset_postdata() ?>
    <section class="reviews" id="reviews">
        <div class="wrapper">
            <h2>Отзывы наших клиентов</h2>
            <ul class="reviewsCarusel owl-carousel">
                <?php $args = array(
                    'offset' => 0,
                    'post_type' => 'reviews',
                    'posts_per_page' => -1); ?>
                <?php $reviews = new WP_query($args); ?>
                <?php while ($reviews->have_posts()) : $reviews->the_post(); ?>
                    <li>
                        <div class="userContainer">
                            <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title(); ?>">
                            <div class="user-title">
                                <img src="<?= get_template_directory_uri() ?>/images/right-quotes-symbol.svg" alt="">
                                <div><p class="name"><?= get_the_title(); ?></p>
                                    <p class="date"><?= get_the_date('d.m.Y') ?></p></div>
                            </div>
                        </div>
                        <p><?= get_the_content(); ?></p>
                    </li>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>
    </section>
    <section class="stock" style="background-image: url('<?= get_field('action_image') ?>')">
        <div class="wrapper">
            <div class="stock-content">
                <p class="stock-date"><?= get_field('action_title') ?></p>
                <p class="stock-description">
                    <?= get_field('action_description') ?>
                </p>
            </div>
            <a href="#formConsultation" class="btn yellow ancLinks">подать заявку</a>
        </div>
    </section>
    <section class="map-container" id="contacts">
        <div class="wrapper">
            <img src="<?= get_template_directory_uri() ?>/images/facebook-placeholder-for-locate-places-on-maps.svg"
                 alt="">
            <h2>Мы находимся</h2>
            <a class="adress" href="https://goo.gl/maps/hVVAnvvMx8L2"><?= get_field('addr') ?></a>
            <?php $phones = get_field('phones', 9); ?>
            <?php if ($phones):
                foreach ($phones as $item): ?>
                    <a href="tel:<?= str_replace(array('-', " ", "(", ")"), '', $item['phone']) ?>"><?= $item['phone'] ?></a>
                <?php endforeach;
            endif; ?>
        </div>
        <div id="map"><img src="<?= get_template_directory_uri() ?>/images/map.png" alt=""></div>
    </section>
<?php $items = get_field('social_networks');
if ($items): ?>
    <section class="social">
        <div class="wrapper">
            <h2>Мы в соц сетях</h2>
            <ul>
                <?php foreach ($items as $item): ?>
                    <li>
                        <a href="<?= $item['link'] ?>"><img src="<?= $item['image'] ?>" alt="<?= $item['link'] ?>"></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
    <?php
endif;
?>
    <section class="seo">
        <div class="wrapper">
            <?= get_field('seo_text') ?>
        </div>
    </section>
<?php
get_footer();

