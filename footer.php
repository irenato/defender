﻿<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.07.17
 * Time: 0:17
 */

?>
<footer>
    <div class="footer-logo">
        <img src="<?= get_template_directory_uri() ?>/images/logo.png" alt="">
    </div>
    <div class="footer-contacts">
        <a href="mailto:d<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
        <?php $phones = get_field('phones', 9); ?>
        <?php if ($phones):
            foreach ($phones as $item): ?>
                <a href="tel:<?= str_replace(array('-', " ", "(", ")"), '', $item['phone']) ?>"><?= $item['phone'] ?></a>
            <?php endforeach;
        endif; ?>
    </div>
</footer>
<!--Callback form and button-->
<div class="message open-form">
    <img src="<?= get_template_directory_uri() ?>/images/chat.svg" alt="chat">
</div>

<form action="" class="form js_sms_form">
    <div class="close-form">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 42 42" style="enable-background:new 0 0 42 42;" xml:space="preserve">
            <rect y="20" width="42" height="2"/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
        </svg>
    </div>
    <h3 class="form__h3"> Оставьте номер телефона - мы перезвоним вам</h3>
    <input id="phone1" name="phone" type="text" placeholder="+38(___)-___-__-__"/>
    <input type="submit" class="phone-submit">
</form>
<!--Callback form and button END-->
<section class="hidden">
    <section class="mobile-menu" id="mobileMenu">
        <div class="wrapper">
            <div class="close-btn" id="mobileMenuClose"><img src="<?= get_template_directory_uri() ?>/images/cross-mark-on-a-black-circle-background.svg" alt=""></div>
            <ul class="menu">
                <li><a href="#aboutUs" class="ancLinks"><img src="<?= get_template_directory_uri() ?>/images/presenter-talking-about-people-on-a-screen.svg" alt="">о нас</a></li>
                <li><a href="#whyWe" class="ancLinks"><img src="<?= get_template_directory_uri() ?>/images/shopping-support-online.svg" alt="">почему мы</a></li>
                <li><a href="#services" class="ancLinks"><img src="<?= get_template_directory_uri() ?>/images/info.svg" alt="">наши услуги</a></li>
                <li><a href="#reviews" class="ancLinks"><img src="<?= get_template_directory_uri() ?>/images/review.svg" alt="">отзывы</a></li>
                <li><a href="#contacts" class="ancLinks"><img src="<?= get_template_directory_uri() ?>/images/business-cards-database.svg" alt="">контакты</a></li>
            </ul>
        </div>
    </section>
</section>
<script>
    var map;
    var image = "<?= get_template_directory_uri() ?>/images/map-marker.svg";
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?= get_field('coords_lt', 9) ?>, lng: <?= get_field('coords_ln', 9) ?>},
            zoom: 16,
            scrollwheel: false,
            styles: [
//                {
//                    "featureType": "landscape.man_made",
//                    "elementType": "geometry",
//                    "stylers": [
//                        {
//                            "color": "#f7f1df"
//                        }
//                    ]
//                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#d0e3b4"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.terrain",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.medical",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#fbd3da"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#bde6ab"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffe15f"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#efd151"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "black"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#cfb2db"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#a2daf2"
                        }
                    ]
                }
            ]
        });
        var beachMarker = new google.maps.Marker({
            position: {lat: <?= get_field('coords_lt', 9) ?>, lng: <?= get_field('coords_ln', 9) ?>},
            map: map,
            icon: image
        });
    }
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104225955-1', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_footer(); ?>
<script src="https://cdnjs.clo