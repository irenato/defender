<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.07.17
 * Time: 0:09
 */

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Полный спектр услуг по аутстаффингу сотрудников и выводу персонала за штат. Лизинг и аренда сотрудников в Украине от компании Defender Ukraine, узнать стоимость и условия вы можете у наших менеджеров.">
    <style>.new_preloader {display: flex;justify-content: center;align-items: center;position: fixed;width: 100vw;height: 100vh;z-index: 99999;background-color: rgba(255, 255, 255, 1);}</style>
    <?php wp_head(); ?>
    <title><?= wp_get_document_title('|', true, 'right'); ?></title>
</head>
<body>
<div class="new_preloader"><img src="<?= get_template_directory_uri() ?>/images/loading.gif" alt="Preloader image"></div>
<header>
    <div class="wrapper">
        <a href="/"><img class="logo" src="<?= get_template_directory_uri() ?>/images/logo.png" alt="Defender"></a>
        <ul class="menu mobile-hidden">
            <li><a href="#aboutUs" class="ancLinks">о нас</a></li>
            <li><a href="#whyWe" class="ancLinks">почему мы</a></li>
            <li><a href="#services" class="ancLinks">наши услуги</a></li>
            <li><a href="#reviews" class="ancLinks">отзывы</a></li>
            <li><a href="#contacts" class="ancLinks">контакты</a></li>
        </ul>

        <ul class="contact-list mobile-hidden">
            <li><a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a></li>
            <?php $phones = get_field('phones', 9); ?>
            <?php if ($phones):
                foreach ($phones as $item): ?>
                    <li><a href="tel:<?= str_replace(array('-', " ", "(", ")"), '', $item['phone']) ?>"><?= $item['phone'] ?></a></li>
                <?php endforeach;
            endif; ?>
        </ul>
        <button class="mobile-menu-btn" id="mobileMenuBtn"><img src="<?= get_template_directory_uri() ?>/images/menu.svg" alt=""></button>
    </div>
</header>
