//AIzaSyBCJX6N-PGyDT2drFx6dbvMhzLljyp4dn4
var map;
var image = "./images/map-marker.svg";
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.997702, lng: 36.236595},
        zoom: 14,
        scrollwheel: false,
        styles: [
            // {
            //     "featureType": "landscape.man_made",
            //     "elementType": "geometry",
            //     "stylers": [
            //         {
            //             "color": "#f7f1df"
            //         }
            //     ]
            // },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d0e3b4"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.medical",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#fbd3da"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#bde6ab"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffe15f"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#efd151"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "black"
                    }
                ]
            },
            {
                "featureType": "transit.station.airport",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#cfb2db"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a2daf2"
                    }
                ]
            }
        ]
    });
    var beachMarker = new google.maps.Marker({
        position: {lat: 49.997702, lng: 36.236595},
        map: map,
        icon: image
    });
}

$(window).on('load', function () {
    var $preloader = $('.new_preloader');
    $preloader.delay(500).fadeOut('slow');
});
$(document).ready(function () {
    if ($('.reviewsCarusel').width() < 769) {
        $('.reviewsCarusel').owlCarousel({
            items: 1,
            loop: true,
            smartSpeed: 450,
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true,
            nav: true,
            navText: ["<img src='http://defender.in.ua/wp-content/themes/defender/images/back.svg' alt=''>"
                , "<img src='http://defender.in.ua/wp-content/themes/defender/images/backright.svg' alt=''>"]
        });
    } else {
        $('.reviewsCarusel').owlCarousel({
            items: 2,
            loop: true,
            smartSpeed: 450,
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true,
            nav: true,
            navText: ["<img src='http://defender.in.ua/wp-content/themes/defender/images/back.svg' alt=''>"
                , "<img src='http://defender.in.ua/wp-content/themes/defender/images/backright.svg' alt=''>"]
        });
    }
    if ($(".mobile-carusel").width() < 769) {
        $(".mobile-carusel").owlCarousel({
            items: 1,
            loop: true,
            smartSpeed: 450,
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true,
            dots: true
        });
    }

    $("a.ancLinks").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        $('html,body').animate({scrollTop: destination}, 1100);
        return false;
    });

    $('#mobileMenuBtn').on('click', function () {
        $('#mobileMenu').show(500);
    })
    $('#mobileMenu').on('click', function () {
        $('#mobileMenu').hide(500);
    })
    $('#mobileMenu ul li a').on('click', function () {
        $('#mobileMenu').hide(500);
    })
    // start button and form
    $('.open-form').on('click', function () {
        $(this).fadeOut(500);
        $('.form').fadeIn(500);
    });

    $('.close-form, .phone-submit').on('click', function (e) {
        $('.form').fadeOut(500);
        $('.open-form').fadeIn(500);
    });

    $('#phone1').mask('+38?(999) 999-99-99');
    $('#phone2').mask('+38?(999) 999-99-99');

    // end button and form

    //sms
    $('.js_sms_form').on('submit', function () {
        var _this = $(this);
        // if($(this).find('input[name=phone]').length > 11)
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: $(_this).serialize() + '&action=' + 'sms_admin',
                success: function(){
                    $(_this).closest('.form').fadeOut(500);
                    $(this).find('input[name=phone]').val('');
                },
            });
        return false;
    })
});